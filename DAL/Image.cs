﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IDAL;

namespace DAL
{
    class Image : IImage
    {
        public void AddImageByUser(SqlParameter[] lp)
        {
            try
            {
                string spName = @"dbo.[addImageByUser]";
                MyDB db = new MyDB();
                db.openDb();
                SqlCommand cmd = new SqlCommand(spName, db.cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(cmd);
                for (int i = 0; i < lp.Length; i++)
                    cmd.Parameters[i + 1].Value = lp[i].Value;
                SqlDataReader dr = cmd.ExecuteReader();
                db.cn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public DataSet GetAllMyImages(SqlParameter[] lp)
        {
            try
            {
                string spName = @"dbo.[getAllMyImages]";
                MyDB db = new MyDB();
                db.openDb();
                SqlCommand cmd = new SqlCommand(spName, db.cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(cmd);
                for (int i = 0; i < lp.Length; i++)
                    cmd.Parameters[i + 1].Value = lp[i].Value;
                //SqlDataReader dr = cmd.ExecuteReader();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet("res");
                da.Fill(ds);
                db.cn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public DataSet GetImageByShared(SqlParameter[] lp)
        {
            try
            {
                string spName = @"dbo.[getImageByShared]";
                MyDB db = new MyDB();
                db.openDb();
                SqlCommand cmd = new SqlCommand(spName, db.cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(cmd);
                for (int i = 0; i < lp.Length; i++)
                    cmd.Parameters[i + 1].Value = lp[i].Value;
                //SqlDataReader dr = cmd.ExecuteReader();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet("res");
                da.Fill(ds);
                db.cn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public DataSet GetImageByUser(SqlParameter[] lp)
        {
            try
            {
                string spName = @"dbo.[getImageByUser]";
                MyDB db = new MyDB();
                db.openDb();
                SqlCommand cmd = new SqlCommand(spName, db.cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(cmd);
                for (int i = 0; i < lp.Length; i++)
                    cmd.Parameters[i + 1].Value = lp[i].Value;
                //SqlDataReader dr = cmd.ExecuteReader();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet("res");
                da.Fill(ds);
                db.cn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public DataSet SearchImagesByCriterions(SqlParameter[] lp)
        {
            try
            {
                string spName = @"dbo.[searchImagesByCriterions]";
                MyDB db = new MyDB();
                db.openDb();
                SqlCommand cmd = new SqlCommand(spName, db.cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(cmd);
                for (int i = 0; i < lp.Length; i++)
                    cmd.Parameters[i + 1].Value = lp[i].Value;
                //SqlDataReader dr = cmd.ExecuteReader();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet("res");
                da.Fill(ds);
                db.cn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
