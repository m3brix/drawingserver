﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    [Dto]
    public class imageDTO
    {
        [identity]
        public int imageId { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string pictuer { get; set; }
        
    }
}
