﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DTO;
using Convert;
using Factory;
using IDAL;

namespace DrawingApi.Controllers
{
    [RoutePrefix("api/Shape")]
    public class ShapeController : ApiController
    {
        
        [Route("AddShapeToImage")]
        public IHttpActionResult AddShapeToImage(List<shapeImageDTO> ls)
        {

            IShape shapes = factory.Resolve<IShape>() as IShape;
            foreach(var s in ls)
                shapes.AddShapeToImage(Convert<shapeImageDTO>.DtoToSqlParameters(s));
            //List<shapeImageDTO> Myshapes = Convert<shapeImageDTO>.DataSetToDTO(res);
            return Ok();
        }
        
        [Route("DeleteShapeFromeImage")]
        public IHttpActionResult DeleteShapeFromeImage(int shapeImageId)
        {
            IShape shapes = factory.Resolve<IShape>() as IShape;
            var res = shapes.DeleteShapeFromeImage(Convert<int>.DtoToSqlParameters(shapeImageId));
            List<shapeImageDTO> Myshapes = Convert<shapeImageDTO>.DataSetToDTO(res);
            return Ok(Myshapes);
        }
        
        [Route("EditShapeInImage")]
        public IHttpActionResult EditShapeInImage(shapeImageDTO s)
        {
            IShape shapes = factory.Resolve<IShape>() as IShape;
            var res = shapes.EditShapeInImage(Convert<shapeImageDTO>.DtoToSqlParameters(s));
            List<shapeImageDTO> Myshapes = Convert<shapeImageDTO>.DataSetToDTO(res);
            return Ok(Myshapes);
        }
        [Route("GetAllShapes")]
        public IHttpActionResult GetAllShapes()
        {
            IShape shapes = factory.Resolve<IShape>() as IShape;
            var res = shapes.GetAllShapes();
            List<shapeDTO> Myshapes = Convert<shapeDTO>.DataSetToDTO(res);
            return Ok(Myshapes);
        }
        
        [Route("GetShapesByImage{userImageId}")]
        public IHttpActionResult GetShapesByImage(int userImageId)
        {
            IShape shapes = factory.Resolve<IShape>() as IShape;
            var res = shapes.GetShapesByImage(Convert<int>.DtoToSqlParameters(userImageId));
            List<shapeImageDTO> Myshapes = Convert<shapeImageDTO>.DataSetToDTO(res);
            return Ok(Myshapes);
        }

    }
}