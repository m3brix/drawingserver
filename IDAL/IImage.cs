﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDAL
{
    public interface IImage
    {
        //void AddImage(SqlParameter[] lp);
        void AddImageByUser(SqlParameter[] lp);
        DataSet GetImageByShared(SqlParameter[] lp);
        DataSet GetImageByUser(SqlParameter[] lp);
        DataSet SearchImagesByCriterions(SqlParameter[] lp);
        DataSet GetAllMyImages(SqlParameter[] lp);
    }
}
