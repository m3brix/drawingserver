﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DTO;
using Convert;
using IDAL;
using Factory;
using System.IO;
using System.Web;
using Newtonsoft.Json.Linq;

namespace DrawingApi.Controllers
{
    [RoutePrefix("api/Image")]
    public class ImageController : ApiController
    {
        System.Web.HttpPostedFile hpf;
        userImageDTO u = new userImageDTO();
        [Route("AddImageByUser")]
        public IHttpActionResult AddImageByUser()
        {
                System.Web.HttpFileCollection hfc = HttpContext.Current.Request.Files;
            hpf = hfc[0];
            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
            string sPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Images/") + Path.GetFileName(hpf.FileName);
            // CHECK IF THE SELECTED FILE ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
            if (!File.Exists(sPath))
                // SAVE THE FILE IN THE FOLDER.
                hpf.SaveAs(sPath);

            string s = "http://localhost:53524/Images/" + hpf.FileName;
            
            var json = HttpContext.Current.Request.Params["userImage"];
            //var jsonContent = Request.Content.ReadAsStringAsync().Result;
            JObject jObject = JObject.Parse(json);
            u.name = jObject["name"].ToString();
            u.description = jObject["description"].ToString();
            u.pictuer = s;
            u.userId = (int)jObject["userId"];
            //foreach(var item in jObject)
            //{
            //    u.=item.Value;
            //}
            
            IImage image = factory.Resolve<IImage>() as IImage;
            image.AddImageByUser(Convert<userImageDTO>.DtoToSqlParameters(u));
            return Ok();
            
            //return Ok(s);
        }
        [Route("GetImageByShared{sharedId}")]
        public IHttpActionResult GetImageByShared(int sharedId)
        {
            IImage image = factory.Resolve<IImage>() as IImage;
            var res = image.GetImageByShared(Convert<int>.DtoToSqlParameters(sharedId));
            List<userImageDTO> images = Convert<userImageDTO>.DataSetToDTO(res);
            return Ok(images);
        }
        [Route("GetImageByUser{userId}")]
        public IHttpActionResult GetImageByUser(int userId)
        {
            IImage image = factory.Resolve<IImage>() as IImage;
            var res = image.GetImageByUser(Convert<int>.DtoToSqlParameters(userId));
            List<userImageDTO> images = Convert<userImageDTO>.DataSetToDTO(res);
            return Ok(images);
        }
        
        [Route("GetAllMyImages{userId}")]
        public IHttpActionResult GetAllMyImages(int userId)
        {
            IImage image = factory.Resolve<IImage>() as IImage;
            var res = image.GetAllMyImages(Convert<int>.DtoToSqlParameters(userId));
            List<userImageDTO> images = Convert<userImageDTO>.DataSetToDTO(res);
            return Ok(images);
        }
        // לטפל בקרטריונים
        //[Route("SearchImagesByCriterions{userId}")]
        //public IHttpActionResult SearchImagesByCriterions(int userId)
        //{

        //}

    }
}
