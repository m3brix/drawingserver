﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace IDAL
{
   public  interface IShape
    {
        DataSet AddShapeToImage (SqlParameter[] lp);
        DataSet DeleteShapeFromeImage (SqlParameter[] lp);
        DataSet EditShapeInImage(SqlParameter[] lp);
        DataSet GetAllShapes();
        DataSet GetShapesByImage(SqlParameter[] lp);

    }
}
