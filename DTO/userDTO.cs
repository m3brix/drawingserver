﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



 namespace DTO
{
    [Dto]
     public class userDTO
    {
       [identity]
        public int userId { get; set; }
        public string lastName { get; set; }
        public string firstName { get; set; }
        public string password { get; set; }
        public string userName { get; set; }
    }
    [Dto]
    public class userLoginDTO
    {
        public string userName { get; set; }
        public string password { get; set; }
    }
}
