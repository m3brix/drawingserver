﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using IDAL;
using System.IO;
using System.Reflection;

namespace DAL
{
    public class User : IUser
    {
        public DataSet AddSharedToImage(SqlParameter[] lp)
        {
            try
            {
                string spName = @"dbo.[addSharedToImage]";


                MyDB db = new MyDB();
                db.openDb();
                SqlCommand cmd = new SqlCommand(spName, db.cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(cmd);
                for (int i = 0; i < lp.Length; i++)
                    cmd.Parameters[i + 1].Value = lp[i].Value;
                //SqlDataReader dr = cmd.ExecuteReader();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet("res");
                da.Fill(ds);
                db.cn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public void AddUser(SqlParameter[] lp)
        {
            try
            {
                string spName = @"dbo.[addUser]";
                MyDB db = new MyDB();
                db.openDb();
                SqlCommand cmd = new SqlCommand(spName, db.cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(cmd);
                for (int i = 0; i < lp.Length; i++)
                    cmd.Parameters[i + 1].Value = lp[i].Value;
                SqlDataReader dr = cmd.ExecuteReader();
                db.cn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public DataSet CheckUser(SqlParameter[] lp)
        {
            try
            {
                string spName = @"dbo.[checkUser]";
                MyDB db = new MyDB();
                db.openDb();
                SqlCommand cmd = new SqlCommand(spName, db.cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(cmd);
                for (int i = 0; i < lp.Length; i++)
                    cmd.Parameters[i + 1].Value = lp[i].Value;
                //SqlDataReader dr = cmd.ExecuteReader();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet("res");
                da.Fill(ds);
                db.cn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public DataSet DeleteSharedFromeImage(SqlParameter[] lp)
        {
            try
            {
                string spName = @"dbo.[deleteSharedFromeImage]";
                MyDB db = new MyDB();
                db.openDb();
                SqlCommand cmd = new SqlCommand(spName, db.cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(cmd);
                for (int i = 0; i < lp.Length; i++)
                    cmd.Parameters[i + 1].Value = lp[i].Value;
                //SqlDataReader dr = cmd.ExecuteReader();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet("res");
                da.Fill(ds);
                db.cn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public DataSet GetAllUsers()
        {
            try
            {
                string spName = @"dbo.[getAllUsers]";
                MyDB db = new MyDB();
                db.openDb();
                SqlCommand cmd = new SqlCommand(spName, db.cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet("res");
                da.Fill(ds);
                db.cn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public DataSet GetSharedsByImage(SqlParameter[] lp)
        {
            try
            {
                string spName = @"dbo.[getSharedsByImage]";
                MyDB db = new MyDB();
                db.openDb();
                SqlCommand cmd = new SqlCommand(spName, db.cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(cmd);
                for (int i = 0; i < lp.Length; i++)
                    cmd.Parameters[i + 1].Value = lp[i].Value;
                //SqlDataReader dr = cmd.ExecuteReader();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet("res");
                da.Fill(ds);
                db.cn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}





