﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DTO;
using Convert;
using IDAL;
using Factory;

namespace DrawingApi.Controllers
{
    [RoutePrefix("api/User")]
    public class UserController : ApiController
    {
        [Route("AddSharedToImage")]
        public IHttpActionResult AddSharedToImage(List<int> userImageIdAndUserId)
        {
            IUser user = factory.Resolve<IUser>() as IUser;
            var res = user.AddSharedToImage(Convert<List<int>>.DtoToSqlParameters(userImageIdAndUserId));
            List<sharedDTO> shareds = Convert<sharedDTO>.DataSetToDTO(res);
            return Ok(shareds);
        }
        [Route("AddUser")]
        public IHttpActionResult AddUser(userDTO u)
        {
            IUser user= factory.Resolve<IUser>() as IUser;
            user.AddUser(Convert<userDTO>.DtoToSqlParameters(u));
            return Ok();
        }
        [Route("CheckUser")]
        public IHttpActionResult CheckUser(List<string> userNameAndPasswors)
        {
            IUser user =factory.Resolve<IUser>() as IUser;
            var res=user.CheckUser(Convert<List<string>>.DtoToSqlParameters(userNameAndPasswors));
            userDTO u = Convert<userDTO>.DataSetToDTO(res)[0];
            return Ok(u);
        }
        
        [Route("DeleteSharedFromeImage")]
        public IHttpActionResult DeleteSharedFromeImage(int sharedId)
        {
            IUser user = factory.Resolve<IUser>() as IUser;
            var res = user.DeleteSharedFromeImage(Convert<int>.DtoToSqlParameters(sharedId));
            List<sharedDTO> u = Convert<sharedDTO>.DataSetToDTO(res);
            return Ok(u);
        }
        
        [Route("GetAllUsers")]
        public IHttpActionResult GetAllUsers()
        {
            IUser user = factory.Resolve<IUser>() as IUser;
            var res = user.GetAllUsers();
            List<sharedDTO> u = Convert<sharedDTO>.DataSetToDTO(res);
            return Ok(u);
        }
        
        [Route("GetSharedsByImage{userImageId}")]
        public IHttpActionResult GetSharedsByImage(int userImageId)
        {
            IUser user = factory.Resolve<IUser>() as IUser;
            var res = user.GetSharedsByImage(Convert<int>.DtoToSqlParameters(userImageId));
            List<sharedDTO> u = Convert<sharedDTO>.DataSetToDTO(res);
            return Ok(u);
        }
    }
}
