﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    [Dto]
    public class shapeImageDTO
    {
        [identity]
        public int shapeImageId { get; set; }
        public int userImageId { get; set; }
        public int shapeId { get; set; }
        public double centerX { get; set; }
        public double centerY { get; set; }
        public double width { get; set; }
        public double height { get; set; }
        public string color { get; set; }
        public string fillingColor { get; set; }
        public  double opacity { get; set; }
        public  double angle { get; set; }
        public string shapeTxt { get; set; }
        public double startX { get; set; }
        public double startY { get; set; }
        public double endX { get; set; }
        public double endY { get; set; }
    }
}
