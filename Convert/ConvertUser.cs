﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using DTO;
using System.Reflection;
using System.Collections;

namespace Convert
{
    public static class Convert<T> 
    {//לא טיפלנו בסוגי הפרמטרים
    //(יכול להיות שצריך להמיר את סוג המאפיין לסוג הפרוצדורה
        public static SqlParameter[] DtoToSqlParameters(T u)
        {
            List<SqlParameter> ls = new List<SqlParameter>();
            //שהוא רשימה T למקרה שנשלח 
            if (u is IEnumerable)
            {
                foreach (var item in (IEnumerable)u)
                {
                    var param = new SqlParameter();
                    param.Value = item;
                    param.SqlDbType = Dictionary.ConvertiTipo(item.GetType());
                    ls.Add(param);
                }
            }
           
            else
            {
                //בדיקה האם הקלאס מכיל אטרביוט של DTO
                bool hasDtoAttribute = u.GetType().GetCustomAttributes(typeof(DtoAttribute), true).Any();
               //אם הוא מסוד DTO
                if (hasDtoAttribute == true)
                {
                    foreach (PropertyInfo propertyInfo in u.GetType().GetProperties())
                    {
                        bool hasIdenityAttribute = propertyInfo.GetCustomAttributes(typeof(identityAttribute), true).Any();
                        if (!hasIdenityAttribute)
                        {
                            var param = new SqlParameter();
                            param.Value = propertyInfo.GetValue(u);
                            param.SqlDbType = Dictionary.ConvertiTipo(propertyInfo.PropertyType);
                            ls.Add(param);
                        }
                    }
                }
                else
                {
                    var param = new SqlParameter();
                    param.Value = u;
                    param.SqlDbType = Dictionary.ConvertiTipo(u.GetType());
                    ls.Add(param);
                }
            }
            return ls.ToArray();
        }

        
        public static List<T> DataSetToDTO(DataSet ds)
        {
            List<T> lDto = new List<T>();
            bool hasDtoAttribute = typeof(T).GetCustomAttributes(typeof(DtoAttribute), true).Any();
            //אם הוא מסוד DTO
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                if (hasDtoAttribute == true)
                {
                    var Instance = Activator.CreateInstance<T>();
                   var properties = Instance.GetType().GetProperties();
                    foreach (var property in properties)
                    {
                        property.SetValue(Instance, row[property.Name]);
                    }
                    lDto.Add(Instance);
                }  
                else
                    foreach(var col in ds.Tables[0].Columns)
                    {
                        var x = (T)row[ds.Tables[0].Columns[0]];
                        lDto.Add(x);
                    }

                    
                }
            return lDto;
        }

    }
}
