﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using DAL;
using IDAL;

namespace DAL
{
    public class Shape : IShape
    {
        public DataSet AddShapeToImage(SqlParameter[] lp)
        {
            try
            {
                string spName = @"dbo.[addShapeToImage]";
                MyDB db = new MyDB();
                db.openDb();
                SqlCommand cmd = new SqlCommand(spName, db.cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(cmd);
                for (int i = 0; i < lp.Length; i++)
                    cmd.Parameters[i + 1].Value = lp[i].Value;
                //SqlDataReader dr = cmd.ExecuteReader();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet("res");
                da.Fill(ds);
                db.cn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public DataSet DeleteShapeFromeImage(SqlParameter[] lp)
        {
            try
            {
                string spName = @"dbo.[deleteShapeFromeImage]";
                MyDB db = new MyDB();
                db.openDb();
                SqlCommand cmd = new SqlCommand(spName, db.cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(cmd);
                for (int i = 0; i < lp.Length; i++)
                    cmd.Parameters[i + 1].Value = lp[i].Value;
                //SqlDataReader dr = cmd.ExecuteReader();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet("res");
                da.Fill(ds);
                db.cn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public DataSet EditShapeInImage(SqlParameter[] lp)
        {
            try
            {
                string spName = @"dbo.[editShapeInImage]";
                MyDB db = new MyDB();
                db.openDb();
                SqlCommand cmd = new SqlCommand(spName, db.cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(cmd);
                for (int i = 0; i < lp.Length; i++)
                    cmd.Parameters[i + 1].Value = lp[i].Value;
                //SqlDataReader dr = cmd.ExecuteReader();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet("res");
                da.Fill(ds);
                db.cn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public DataSet GetAllShapes()
        {
            try
            {
                string spName = @"dbo.[getAllShapes]";
                MyDB db = new MyDB();
                db.openDb();
                SqlCommand cmd = new SqlCommand(spName, db.cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet("res");
                da.Fill(ds);
                db.cn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public DataSet GetShapesByImage(SqlParameter[] lp)
        {
            try
            {
                string spName = @"dbo.[getShapesByImage]";
                MyDB db = new MyDB();
                db.openDb();
                SqlCommand cmd = new SqlCommand(spName, db.cn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlCommandBuilder.DeriveParameters(cmd);
                for (int i = 0; i < lp.Length; i++)
                    cmd.Parameters[i + 1].Value = lp[i].Value;
                //SqlDataReader dr = cmd.ExecuteReader();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                DataSet ds = new DataSet("res");
                da.Fill(ds);
                db.cn.Close();
                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}





