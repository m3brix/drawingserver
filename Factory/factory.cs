﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Factory
{
    //public sealed class Singleton
    //{
    //    private static Singleton instance = null;

    //    private Singleton()
    //    {
    //    }

    //    public static Singleton Instance
    //    {
    //        get
    //        {
    //            if (instance == null)
    //            {
    //                instance = new Singleton();
    //            }
    //            return instance;
    //        }
    //    }
        public static class factory
    {

           public static List<Type> classes = new List<Type>();
           public static Dictionary<Type, Type> dicTypes = new Dictionary<Type, Type>();
           public static Assembly a { get; set; }

        public static void LoadAssemblies(string pathToDllsFile)
        {
            var files = Directory.GetFiles(pathToDllsFile, "*.dll", SearchOption.AllDirectories);
            //@"C:\Projects\DevPlusPlus\drawingapi\DAL\bin\Debug
            foreach(string file in files)
            {
              a = Assembly.LoadFrom(file);
              classes.AddRange(a.GetTypes().Where(c => c.IsClass));
            }
           
          
        }

        public static object Resolve<T>()
        {
            
            Type t = typeof(T);
            dicTypes[t] = classes.FirstOrDefault(c => c.GetInterface(t.ToString())!=null);
            var Instance = Activator.CreateInstance(dicTypes[t]);
            return Instance;
        }

    }
}
