﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace IDAL
{
    public interface IUser
    {
        void AddUser(SqlParameter[] lp);
        DataSet CheckUser(SqlParameter[] lp);
        DataSet GetAllUsers();
        DataSet AddSharedToImage(SqlParameter[] lp);
        DataSet DeleteSharedFromeImage(SqlParameter[] lp);
        DataSet GetSharedsByImage(SqlParameter[] lp);

    }
}
