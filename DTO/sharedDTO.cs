﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{

    [Dto]
    public class sharedDTO
    {
        [identity]
        public int sharedId{ get; set; }
        public int userImageId { get; set; }
        public int userId { get; set; }
        public string lastName { get; set; }
        public string firstName { get; set; }
    }
}
