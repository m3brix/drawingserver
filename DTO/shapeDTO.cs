﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    [Dto]
    public class shapeDTO
    {
        [identity]
        public int shapeId { get; set; }
        public string shapeName { get; set; }
        public shapeDTO()
        {

        }
        public shapeDTO(int shapeId, string shapeName)
        {
            this.shapeId = shapeId;
            this.shapeName = shapeName;
        }
     
    }
}
