﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
   
  [AttributeUsage(AttributeTargets.Property)]
  public class identityAttribute: Attribute { }

    [AttributeUsage(AttributeTargets.Class)]
    public class DtoAttribute : Attribute { }
}
